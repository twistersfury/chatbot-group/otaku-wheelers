#include <catch2/catch_all.hpp>
#include <filesystem>
#include <fstream>
#include <OtakuWheelers/DiscordBot/Config/Loader.hpp>
#include <OtakuWheelers/DiscordBot/Config/Exception/NotFound.hpp>

using std::filesystem::temp_directory_path;
using std::ofstream;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::NotFound;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Loader;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Config::Loader") {
    string tempPath = temp_directory_path();

    SECTION("Path Not Found") {
        REQUIRE_THROWS_AS(
            ([]() {
                auto testSubject = Loader{temp_directory_path().string() + "/file-does-not-exist"};
            })(),
            NotFound
        );
    }

    SECTION("Path Found") {
        string filePath = temp_directory_path().string() + "/file.json";

        ofstream tempFile;
        tempFile.open(filePath);
        tempFile << R"({"something": "else"})";
        tempFile.close();

        auto testSubject = Loader{filePath};
        REQUIRE(testSubject.loadConfig()["something"] == "else");
    }
}
