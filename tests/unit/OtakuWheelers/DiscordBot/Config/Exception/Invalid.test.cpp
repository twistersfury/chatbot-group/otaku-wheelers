#include <catch2/catch_all.hpp>

#include <OtakuWheelers/DiscordBot/Config/Exception/Invalid.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::Invalid;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::Invalid") {
    SECTION("Default Constructor") {
        auto testSubject = Invalid{};

        REQUIRE(strcmp(testSubject.what(), "Invalid Configuration") == 0);
    }

    SECTION("Alt Constructor") {
        auto testSubject = Invalid{"Something"};

        REQUIRE(strcmp(testSubject.what(), "Something") == 0);
    }
}
