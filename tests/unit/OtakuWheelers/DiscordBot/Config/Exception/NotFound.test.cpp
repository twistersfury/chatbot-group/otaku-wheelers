#include <catch2/catch_all.hpp>
#include <OtakuWheelers/DiscordBot/Config/Exception/NotFound.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::NotFound;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::NotFound") {
    SECTION("Default Constructor") {
        auto testSubject = NotFound{};

        REQUIRE(strcmp(testSubject.what(), "Configuration File Not Found") == 0);
    }

    SECTION("Alt Constructor") {
        auto testSubject = NotFound{"Something"};

        REQUIRE(strcmp(testSubject.what(), "Something") == 0);
    }
}
