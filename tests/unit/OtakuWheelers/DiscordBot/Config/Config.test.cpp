#include <catch2/catch_all.hpp>
#include <OtakuWheelers/DiscordBot/Config/Config.hpp>
#include <OtakuWheelers/DiscordBot/Config/Exception/Invalid.hpp>


using TwistersFury::OtakuWheelers::DiscordBot::Config::Config;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::Invalid;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Config::Config") {
    json testData = {
        {"token", "abc123"},
        {"api_url", "blah"}
    };

    SECTION("Invalid Configuration - Token") {
        REQUIRE_THROWS_AS(
            ([]() {
                json testData = {
                    {"no_token", ""}
                };

                auto testSubject = Config{testData};
            })(),
            Invalid
        );
    }

    SECTION("Get Token") {
        auto testSubject = Config{testData};

        REQUIRE(testSubject.getToken() == "abc123");
    }

    SECTION("Get API URL") {
        auto testSubject = Config{testData};

        REQUIRE(testSubject.getApiBaseUrl() == "blah");
    }
}
