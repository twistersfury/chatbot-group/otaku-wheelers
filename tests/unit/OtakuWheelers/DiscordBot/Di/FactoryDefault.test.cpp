#include <catch2/catch_all.hpp>
#include <fakeit.hpp>

#include <OtakuWheelers/DiscordBot/Di/FactoryDefault.hpp>

using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Di::FactoryDefault;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Di::FactoryDefault") {
    Mock<ConfigInterface> configMocker;

    Fake(Method(configMocker, getApiBaseUrl));
    Fake(Method(configMocker, getToken));

    auto testSubject = FactoryDefault{configMocker.get()};

    SECTION("Test Get Command Manager") {
        REQUIRE(testSubject.getCommandManager().getCommands().size() > 0);
    }

    SECTION("Getting Config") {
        REQUIRE(&testSubject.getConfig() == &configMocker.get());
    }

    SECTION("Getting Cluster") {
        testSubject.getCluster();
        testSubject.getCluster();
    }

    SECTION("Getting Application") {
        testSubject.getApplication();
        testSubject.getApplication();
    }
}
