#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/PingPong.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::PingPong;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::PingPong") {
    PingPong pingPong = PingPong{};

    SECTION("It Gets The Basics") {
        REQUIRE(pingPong.getCommand() == "ping");
        REQUIRE(pingPong.getDescription() == "Ping pong!");
    }

    SECTION("It Fires The Event") {
        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("Pong!")).AlwaysReturn();

        pingPong.handleEvent(commandMocker.get());

        Verify(Method(commandMocker, reply).Using("Pong!")).Once();
    }
}