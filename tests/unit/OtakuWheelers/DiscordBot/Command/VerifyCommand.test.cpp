#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/VerifyCommand.hpp>
#include <OtakuWheelers/DiscordBot/Api/Request.hpp>

using namespace dpp;

using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::VerifyCommand;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Request;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::VerifyCommand") {
    Mock<VerifyRequestInterface> requestMocker;
    Mock<VerifyResponseInterface> responseMocker;

    When(Method(responseMocker, isVerified)).Return(true);
    When(Method(requestMocker, verifyUser)).AlwaysReturn(&responseMocker.get());

    auto testSubject = VerifyCommand{ requestMocker.get() };

    SECTION("It Gets The Basics") {
        REQUIRE(testSubject.getCommand() == "verify");
        REQUIRE(testSubject.getDescription() == "Website Verification");
    }

    SECTION("It Fires The Event") {
        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("Thank You! Your verification is complete!")).AlwaysReturn();
        When(Method(commandMocker, getParameter)).AlwaysReturn("123456");

        testSubject.handleEvent(commandMocker.get());

        fakeit::Verify(Method(commandMocker, reply).Using("Thank You! Your verification is complete!")).Once();
    }

    SECTION("It Fires The Event - Code") {
        When(Method(responseMocker, isVerified)).AlwaysReturn(false);
        When(Method(responseMocker, getError)).AlwaysReturn(Errors::Code);
        When(Method(requestMocker, verifyUser)).AlwaysReturn(&responseMocker.get());

        auto testSubject = VerifyCommand{ requestMocker.get() };

        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("Invalid Verification Code")).AlwaysReturn();
        When(Method(commandMocker, getParameter)).AlwaysReturn("123456");

        testSubject.handleEvent(commandMocker.get());

        fakeit::Verify(Method(commandMocker, reply).Using("Invalid Verification Code")).Once();
    }

    SECTION("It Fires The Event - Invalid") {
        When(Method(responseMocker, isVerified)).AlwaysReturn(false);
        When(Method(responseMocker, getError)).AlwaysReturn(Errors::Invalid);
        When(Method(requestMocker, verifyUser)).AlwaysReturn(&responseMocker.get());

        auto testSubject = VerifyCommand{ requestMocker.get() };

        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("You've submitted an invalid verification code.")).AlwaysReturn();
        When(Method(commandMocker, getParameter)).AlwaysReturn("123456");

        testSubject.handleEvent(commandMocker.get());

        fakeit::Verify(Method(commandMocker, reply).Using("You've submitted an invalid verification code.")).Once();
    }

    SECTION("It Fires The Event - NotFound") {
        When(Method(responseMocker, isVerified)).AlwaysReturn(false);
        When(Method(responseMocker, getError)).AlwaysReturn(Errors::NotFound);
        When(Method(requestMocker, verifyUser)).AlwaysReturn(&responseMocker.get());

        auto testSubject = VerifyCommand{ requestMocker.get() };

        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("You do not appear to have any pending verifications.")).AlwaysReturn();
        When(Method(commandMocker, getParameter)).AlwaysReturn("123456");

        testSubject.handleEvent(commandMocker.get());

        fakeit::Verify(Method(commandMocker, reply).Using("You do not appear to have any pending verifications.")).Once();
    }

    SECTION("It Fires The Event - Default") {
        When(Method(responseMocker, isVerified)).AlwaysReturn(false);
        When(Method(responseMocker, getError)).AlwaysReturn(Errors::None);
        When(Method(requestMocker, verifyUser)).AlwaysReturn(&responseMocker.get());

        auto testSubject = VerifyCommand{ requestMocker.get() };

        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply).Using("An unrecognized error has occurred.")).AlwaysReturn();
        When(Method(commandMocker, getParameter)).AlwaysReturn("123456");

        testSubject.handleEvent(commandMocker.get());

        fakeit::Verify(Method(commandMocker, reply).Using("An unrecognized error has occurred.")).Once();
    }
}
