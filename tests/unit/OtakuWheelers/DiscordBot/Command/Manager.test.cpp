#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/Manager.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::Manager;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::Manager") {
    Mock<ConfigInterface> configMocker;

    When(Method(configMocker, getApiBaseUrl)).AlwaysReturn("http://localhost:8081");

    auto testSubject = Manager{configMocker.get()};

    testSubject.initialize();

    SECTION("It Can Add a Command") {
        Mock<CommandInterface> commandMocker;

        When(Method(commandMocker,getCommand)).AlwaysReturn("command");

        CommandInterface* mockCommand = &commandMocker.get();

        testSubject.addCommand(mockCommand);

        REQUIRE(testSubject.getCommand("command").getCommand() == "command");
    }

    SECTION("It Can Get Commands") {
        REQUIRE(testSubject.getCommands().size() == 3); //3 Is Default Build Options
    }
}
