#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/PingPong.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::PingPong;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::AbstractCommand") {
    auto testSubject = PingPong{};

    SECTION("It Gets The Basics") {
        REQUIRE(testSubject.getOptions().size() == 0);
    }
}
