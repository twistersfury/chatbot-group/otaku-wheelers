#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/Version.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::Version;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::Version") {
    auto testSubject = Version{};

    SECTION("It Gets The Basics") {
        REQUIRE(testSubject.getCommand() == "version");
        REQUIRE(testSubject.getDescription() == "Retrieve Current Bot Version");
    }

    SECTION("It Fires The Event") {
        Mock<SlashCommandInterface> commandMocker;

        When(Method(commandMocker, reply)).AlwaysReturn();

        testSubject.handleEvent(commandMocker.get());
    }
}