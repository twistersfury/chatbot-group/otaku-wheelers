#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Command/Option/TwoFactorOption.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Command::Option::TwoFactorOption;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Command::Option::TwoFactorOption") {
    auto testSubject = TwoFactorOption{};

    SECTION("It Gets The Basics") {
        REQUIRE(testSubject.getName() == "code");
        REQUIRE(testSubject.getDescription() == "Verification Code From Website");
        REQUIRE(testSubject.isRequired() == true);
        REQUIRE(testSubject.getValues().size() == 0);
    }
}
