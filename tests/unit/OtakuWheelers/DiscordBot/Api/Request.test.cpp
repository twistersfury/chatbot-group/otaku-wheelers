#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Api/Request.hpp>

using namespace fakeit;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Request;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Api::Request") {
    Mock<CurlInterface> curlMocker;

    When(Method(curlMocker, postRequest)).AlwaysReturn("{\"verified\":true,\"error\":0}");

    auto testSubject = Request{"http://localhost:8081", &curlMocker.get()};

    SECTION("Test Verify Request") {
        REQUIRE(testSubject.verifyUser("someUser", "123456")->isVerified() == true);
    }

    SECTION("Test Normal Init") {
        auto altTest = Request{"http://localhost:8081"};
    }

    SECTION("Test Default Make Request") {
        REQUIRE(testSubject.makeRequest("/some/path")["verified"] == true);
    }
}
