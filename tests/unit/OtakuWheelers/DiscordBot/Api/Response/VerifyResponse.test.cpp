#include <catch2/catch_all.hpp>
#include <OtakuWheelers/DiscordBot/Api/Response/VerifyResponse.hpp>
#include <nlohmann/json.hpp>


using TwistersFury::OtakuWheelers::DiscordBot::Api::Response::VerifyResponse;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Api::Response::VerifyResponse") {
    json jsonData = {
        {"verified", true},
        {"error", 0}
    };

    auto testSubject = VerifyResponse{jsonData};

    SECTION("Basic Functionality Works") {
        REQUIRE(testSubject.isVerified() == true);
        REQUIRE(testSubject.getError() == Errors::None);
    }

    SECTION("Test Copy Works") {
        auto copy = VerifyResponse(testSubject);

        REQUIRE(copy.isVerified() == true);
    }
}
