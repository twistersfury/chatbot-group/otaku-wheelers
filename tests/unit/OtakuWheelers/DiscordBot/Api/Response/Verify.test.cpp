#include <catch2/catch_all.hpp>
#include <OtakuWheelers/DiscordBot/Api/Response/VerifyResponse.hpp>
#include <nlohmann/json.hpp>


using TwistersFury::OtakuWheelers::DiscordBot::Api::Response::VerifyResponse;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Api::Response::Verify") {
    SECTION("Basic Functionality Works") {
        json jsonData = {
            {"verified", true},
            {"error", Errors::None}
        };

        auto testSubject = VerifyResponse{jsonData};
        REQUIRE(testSubject.isVerified() == true);
    }
}
