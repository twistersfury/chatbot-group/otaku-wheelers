#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Application/Application.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Application::Application;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Application") {
    Mock<CommandInterface> commandMocker;

    Fake(Method(commandMocker, getCommand));
    Fake(Method(commandMocker, getDescription));
    Fake(Method(commandMocker, getOptions));
    Fake(Method(commandMocker, handleEvent));

    Mock<ConfigInterface> configMocker;
    Mock<ClusterInterface> clusterMocker;
    Mock<ManagerInterface> managerMocker;

    auto commands = map<string,CommandInterface*>{};
    commands["command"] = &commandMocker.get();

    When(Method(managerMocker, getCommands)).AlwaysReturn(commands);
    When(Method(managerMocker, getCommand)).AlwaysReturn(commandMocker.get());

    Fake(Method(clusterMocker, getId));
    Fake(Method(clusterMocker, global_command_create));
    Fake(Method(clusterMocker, on_log));
    Fake(Method(clusterMocker, on_ready));
    Fake(Method(clusterMocker, on_slashcommand));
    Fake(Method(clusterMocker, start));

    When(Method(configMocker, getToken)).AlwaysReturn("abc123");
    When(Method(configMocker, getApiBaseUrl)).AlwaysReturn("http://localhost:8081");

    Mock<LoggerInterface> mockLogger;

    Fake(Method(mockLogger, info));

    Mock<DiInterface> diMocker;

    When(Method(diMocker, getConfig)).AlwaysReturn(configMocker.get());
    When(Method(diMocker, getCluster)).AlwaysReturn(clusterMocker.get());
    When(Method(diMocker, getCommandManager)).AlwaysReturn(managerMocker.get());
    When(Method(diMocker, getLogger)).AlwaysReturn(mockLogger.get());

    auto testSubject = Application{diMocker.get()};

    SECTION("Runs Bot") {
        When(Method(clusterMocker, on_slashcommand)).AlwaysDo([](std::function<void(const slashcommand_t&)> command) {
            command(slashcommand_t{});
        });

        When(Method(clusterMocker, on_ready)).AlwaysDo([](std::function<void(const ready_t&)> command) {
            command(ready_t{});
        });

        When(Method(clusterMocker, start_timer)).AlwaysDo([](timer_callback_t on_tick, uint64_t frequency, timer_callback_t on_stop) -> long {
            on_tick(timer{});

            return 0;
        });

        testSubject.initialize();
        testSubject.run();
        testSubject.onReady(); //Extra Call To Register Ready Line

        Verify(Method(clusterMocker, start).Using(false)).Once();
        Verify(Method(clusterMocker, on_ready)).Once();
        Verify(Method(clusterMocker, on_slashcommand)).Once();

        Verify(Method(commandMocker, handleEvent)).Once();
        Verify(Method(clusterMocker, global_command_create)).Once();
    }

    SECTION("Runs Bot - Direct") {
        When(Method(clusterMocker, on_slashcommand)).AlwaysDo([](std::function<void(const slashcommand_t&)> command) {
            command(slashcommand_t{});
        });

        When(Method(clusterMocker, on_ready)).AlwaysDo([](std::function<void(const ready_t&)> command) {
            command(ready_t{});
        });

        When(Method(clusterMocker, start_timer)).AlwaysDo([](timer_callback_t on_tick, uint64_t frequency, timer_callback_t on_stop) -> long {
            on_tick(timer{});

            return 0;
        });

        testSubject.initialize();
        testSubject.run();
        testSubject.onReady(); //Extra Call To Register Ready Line

        Verify(Method(clusterMocker, start).Using(false)).Once();
        Verify(Method(clusterMocker, on_ready)).Once();
        Verify(Method(clusterMocker, on_slashcommand)).Once();

        Verify(Method(commandMocker, handleEvent)).Once();
        Verify(Method(clusterMocker, global_command_create)).Once();
    }

    SECTION("Slash Commands Run") {
        Mock<SlashCommandInterface> slashMocker;

        When(Method(slashMocker,getName)).AlwaysReturn("ping");
        Fake(Method(slashMocker,reply));

        testSubject.onSlash(slashMocker.get());

        Verify(Method(commandMocker, handleEvent)).Once();
    }
}
