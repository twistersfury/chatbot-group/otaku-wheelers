#include <catch2/catch_all.hpp>
#include <fakeit.hpp>
#include <OtakuWheelers/DiscordBot/Dpp/CommandFactory.hpp>

using namespace dpp;
using namespace fakeit;

using TwistersFury::OtakuWheelers::DiscordBot::Dpp::CommandFactory;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Dpp::CommandFactory") {
    Mock<CommandInterface> commandMocker;
    Mock<ClusterInterface> clusterMocker;

    auto optionsMap = map<string, OptionInterface*>{};

    When(Method(commandMocker, getCommand)).AlwaysReturn("command");
    When(Method(commandMocker, getDescription)).AlwaysReturn("description");
    When(Method(commandMocker, getOptions)).AlwaysReturn(optionsMap);
    Fake(Method(clusterMocker, getId));

    auto testSubject = CommandFactory{};

    SECTION("Build Empty Command") {
        auto command = testSubject.makeCommand(commandMocker.get(), clusterMocker.get());

        REQUIRE(command.name == "command");
        REQUIRE(command.description == "description");

        REQUIRE(command.options.size() == 0);
    }

    SECTION("Build Option - Enter") {
        Mock<OptionInterface> optionMocker;

        When(Method(optionMocker, getName)).AlwaysReturn("option");
        When(Method(optionMocker, getDescription)).AlwaysReturn("option description");
        When(Method(optionMocker, getValues)).AlwaysReturn(map<string, string>{});
        When(Method(optionMocker, isRequired)).AlwaysReturn(true);

        optionsMap["option"] = &optionMocker.get();

        When(Method(commandMocker, getOptions)).AlwaysReturn(optionsMap);

        auto command = testSubject.makeCommand(commandMocker.get(), clusterMocker.get());

        REQUIRE(command.options.size() == 1);
        REQUIRE(command.options.front().name == "option");
        REQUIRE(command.options.front().description == "option description");
        REQUIRE(command.options.front().options.size() == 0);
    }

    SECTION("Build Option - Choice") {
        auto optionChoices = map<string, string>{};
        optionChoices["something"] = "else";

        Mock<OptionInterface> optionMocker;

        When(Method(optionMocker, getName)).AlwaysReturn("option");
        When(Method(optionMocker, getDescription)).AlwaysReturn("option description");
        When(Method(optionMocker, isRequired)).AlwaysReturn(true);
        When(Method(optionMocker, getValues)).AlwaysReturn(optionChoices);

        optionsMap["option"] = &optionMocker.get();

        When(Method(commandMocker, getOptions)).AlwaysReturn(optionsMap);

        auto command = testSubject.makeCommand(commandMocker.get(), clusterMocker.get());

        REQUIRE(command.options.size() == 1);
        REQUIRE(command.options.front().choices.size() == 1);

        REQUIRE(command.options.front().choices.front().name == "something");
    }
}
