#include <catch2/catch_all.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <OtakuWheelers/DiscordBot/Logger/StandardOut.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Logger::StandardOut;

TEST_CASE("TwistersFury::OtakuWheelers::DiscordBot::Logger::StandardOut") {
    auto testSubject = StandardOut{};

    std::stringstream buffer;

    // Redirect std::cout to buffer
    std::streambuf* prevcoutbuf = std::cout.rdbuf(buffer.rdbuf());

    SECTION("Testing Alert") {
        testSubject.alert("Alert Message");

        REQUIRE(buffer.str().find("] ALERT: Alert Message", 0) != std::string::npos);
    }

    SECTION("Testing Debug") {
        testSubject.debug("Debug Message");

        REQUIRE(buffer.str().find("] DEBUG: Debug Message", 0) != std::string::npos);
    }

    SECTION("Testing EMERGENCY") {
        testSubject.emergency("EMERGENCY Message");

        REQUIRE(buffer.str().find("] EMERGENCY: EMERGENCY Message", 0) != std::string::npos);
    }

    SECTION("Testing ERROR") {
        testSubject.error("ERROR Message");

        REQUIRE(buffer.str().find("] ERROR: ERROR Message", 0) != std::string::npos);
    }

    SECTION("Testing INFO") {
        testSubject.info("INFO Message");

        REQUIRE(buffer.str().find("] INFO: INFO Message", 0) != std::string::npos);
    }

    SECTION("Testing NOTICE") {
        testSubject.notice("NOTICE Message");

        REQUIRE(buffer.str().find("] NOTICE: NOTICE Message", 0) != std::string::npos);
    }

    SECTION("Testing WARNING") {
        testSubject.warning("WARNING Message");

        REQUIRE(buffer.str().find("] WARNING: WARNING Message", 0) != std::string::npos);
    }
}
