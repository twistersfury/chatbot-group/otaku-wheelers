//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CONFIG_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CONFIG_HPP

#include <nlohmann/json.hpp>
#include <OtakuWheelers/DiscordBot/Config/Interface/ConfigInterface.hpp>
#include <string>

using nlohmann::json;
using std::string;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Interface::ConfigInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    class Config: public ConfigInterface
    {
        json configData = {};
    protected:
        const string KEY_TOKEN = "token";
        const string KEY_API_URL = "api_url";
    public:
        explicit Config(json configData);

        string getToken() override;
        string getApiBaseUrl() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CONFIG_HPP
