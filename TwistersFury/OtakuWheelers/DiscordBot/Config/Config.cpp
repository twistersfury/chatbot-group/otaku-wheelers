//
// Created by Fenikkusu on 11/6/23.
//

#include "Config.hpp"
#include <OtakuWheelers/DiscordBot/Config/Exception/Invalid.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::Invalid;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    Config::Config(json configData)
    {
        if (!configData.contains(Config::KEY_TOKEN)) {
            throw Invalid();
        }

        this->configData = configData;
    }

    string Config::getToken()
    {
        return this->configData[Config::KEY_TOKEN];
    }

    string Config::getApiBaseUrl() {
        return this->configData[Config::KEY_API_URL];
    }
}
