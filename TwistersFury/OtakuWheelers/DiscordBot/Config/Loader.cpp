//
// Created by Fenikkusu on 11/6/23.
//

#include "Loader.hpp"
#include <filesystem>
#include <fstream>
#include <OtakuWheelers/DiscordBot/Config/Exception/NotFound.hpp>

using std::filesystem::exists;
using std::ifstream;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Exception::NotFound;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    Loader::Loader(string configPath)
    {
        if (!exists(configPath)) {
            throw NotFound();
        }

        this->configPath = configPath;
    }

    json Loader::loadConfig()
    {
        ifstream fileStream(this->configPath);

        return json::parse(fileStream);
    }
}
