//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CONFIGINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CONFIGINTERFACE_HPP

#include <string>

using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config::Interface {
    struct ConfigInterface {
        virtual string getToken() =0;
        virtual string getApiBaseUrl() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CONFIGINTERFACE_HPP
