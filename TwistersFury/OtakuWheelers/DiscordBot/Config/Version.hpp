//
// Created by Fenikkusu on 11/7/23.
//

#include <string>

#include "../../../RawVersion.hpp"

using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    class Version {
        RawVersion* version;
    public:
        Version();
        string getVersion();
    };
}
