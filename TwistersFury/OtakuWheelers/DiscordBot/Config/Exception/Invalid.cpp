//
// Created by Fenikkusu on 11/6/23.
//

#include "Invalid.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Config::Exception {
    Invalid::Invalid(std::string message): runtime_error(std::move(message))
    {

    }

    Invalid::Invalid(): Invalid("Invalid Configuration") {

    }
}
