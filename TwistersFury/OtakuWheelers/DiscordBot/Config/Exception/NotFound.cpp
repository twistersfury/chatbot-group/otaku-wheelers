//
// Created by Fenikkusu on 11/6/23.
//

#include "NotFound.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Config::Exception {
    NotFound::NotFound(std::string message): runtime_error(std::move(message))
    {

    }

    NotFound::NotFound(): NotFound("Configuration File Not Found") {

    }
}
