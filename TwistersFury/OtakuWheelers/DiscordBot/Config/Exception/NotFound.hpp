//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CONFIG_NOTFOUND_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CONFIG_NOTFOUND_HPP

#include <stdexcept>
#include <string>

using std::runtime_error;
using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config::Exception {
    class NotFound: public runtime_error {
    public:
        NotFound();
        explicit NotFound(string message);
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CONFIG_NOTFOUND_HPP
