//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CONFIG_INVALID_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CONFIG_INVALID_HPP

#include <stdexcept>
#include <string>

using std::runtime_error;
using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config::Exception {
    class Invalid: public runtime_error {
    public:
        Invalid();
        explicit Invalid(string message);
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CONFIG_INVALID_HPP
