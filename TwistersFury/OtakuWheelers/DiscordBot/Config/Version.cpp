//
// Created by Fenikkusu on 11/7/23.
//

#include "Version.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    Version::Version()
    {
        this->version = new RawVersion();
    }

    string Version::getVersion()
    {
        return this->version->major + "." + this->version->minor + "." + this->version->patch;
    }
}
