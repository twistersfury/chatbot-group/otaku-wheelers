//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CONFIG_LOADER_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CONFIG_LOADER_HPP

#include <string>
#include <nlohmann/json.hpp>

using nlohmann::json;
using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Config {
    class Loader {
        string configPath = "";
    public:
        explicit  Loader(string configPath);

        json loadConfig();
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CONFIG_LOADER_HPP
