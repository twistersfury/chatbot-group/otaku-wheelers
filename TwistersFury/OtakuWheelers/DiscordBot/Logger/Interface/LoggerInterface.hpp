//
// Created by Fenikkusu on 11/15/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_LOGGERINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_LOGGERINTERFACE_HPP

#include <nlohmann/json.hpp>
#include <optional>
#include <string>

using std::string;
using std::optional;
using std::nullopt;
using nlohmann::json;

namespace TwistersFury::OtakuWheelers::DiscordBot::Logger::Interface {
    enum LogLevel {
        Alert,
        Debug,
        Emergency,
        Error,
        Info,
        Notice,
        Warning
    };

    struct LoggerInterface {
        virtual void alert(string message, optional<json> context = nullopt) =0;
        virtual void debug(string message, optional<json> context = nullopt) =0;
        virtual void emergency(string message, optional<json> context = nullopt) =0;
        virtual void error(string message, optional<json> context = nullopt) =0;
        virtual void info(string message, optional<json> context = nullopt) =0;
        virtual void notice(string message, optional<json> context = nullopt) =0;
        virtual void warning(string message, optional<json> context = nullopt) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_LOGGERINTERFACE_HPP
