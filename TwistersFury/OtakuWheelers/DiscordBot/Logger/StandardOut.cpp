//
// Created by Fenikkusu on 11/15/23.
//

#include "StandardOut.hpp"
#include <iostream>
#include <fstream>
#include "dpp/utility.h"

using dpp::utility::current_date_time;
using std::cout;
using std::ofstream;

namespace TwistersFury::OtakuWheelers::DiscordBot::Logger {
    void StandardOut::log(LogLevel level, const string& message, optional<nlohmann::json> context)
    {
        this->updateCounter();

        string jsonContext = string{};
        if (context.has_value()) {
            jsonContext = context.value().dump();
        }

        cout << "[" << current_date_time() << "] " << this->convertLevel(level) << ": " << message << " :: " << jsonContext <<  std::endl;
    }

    void StandardOut::alert(std::string message, optional<nlohmann::json> context)
    {
        this->log(LogLevel::Alert, message, context);
    }

    void StandardOut::debug(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Debug, message, context);
    }

    void StandardOut::emergency(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Emergency, message, context);
    }

    void StandardOut::error(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Error, message, context);
    }

    void StandardOut::info(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Info, message, context);
    }

    void StandardOut::notice(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Notice, message, context);
    }

    void StandardOut::warning(std::string message, optional<nlohmann::json> context) {
        this->log(LogLevel::Warning, message, context);
    }

    string StandardOut::convertLevel(const LogLevel level)
    {
        switch (level) {
            case LogLevel::Alert:
                return "ALERT";
            case LogLevel::Debug:
                return "DEBUG";
            case LogLevel::Emergency:
                return "EMERGENCY";
            case LogLevel::Error:
                return "ERROR";
            case LogLevel::Info:
                return "INFO";
            case LogLevel::Notice:
                return "NOTICE";
            case LogLevel::Warning:
                return "WARNING";
        }
    }

    void StandardOut::updateCounter()
    {
        this->logCounter += 1;

        ofstream fileCounter;

        fileCounter.open("/tmp/tf-health-check");
        fileCounter.clear();
        fileCounter << this->logCounter << std::endl;
        fileCounter.close();
    }

}
