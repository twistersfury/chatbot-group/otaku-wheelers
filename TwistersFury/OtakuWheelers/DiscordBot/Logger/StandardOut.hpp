//
// Created by Fenikkusu on 11/15/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_STANDARDOUT_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_STANDARDOUT_HPP

#include "Interface/LoggerInterface.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Logger::Interface::LoggerInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Logger::Interface::LogLevel;


namespace TwistersFury::OtakuWheelers::DiscordBot::Logger {
    class StandardOut: public LoggerInterface {
        int logCounter = 0;

        string convertLevel(LogLevel level);

        void updateCounter();

    public:
        void log(LogLevel level, const string& message, optional<json> context = nullopt);

        void alert(string message, optional<json> context = nullopt) override;
        void debug(string message, optional<json> context = nullopt) override;
        void emergency(string message, optional<json> context = nullopt) override;
        void error(string message, optional<json> context = nullopt) override;
        void info(string message, optional<json> context = nullopt) override;
        void notice(string message, optional<json> context = nullopt) override;
        void warning(string message, optional<json> context = nullopt) override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_STANDARDOUT_HPP
