//
// Created by Fenikkusu on 11/8/23.
//

#include "FactoryDefault.hpp"

#include <OtakuWheelers/DiscordBot/Application/Application.hpp>
#include <OtakuWheelers/DiscordBot/Command/Manager.hpp>
#include <OtakuWheelers/DiscordBot/Logger/StandardOut.hpp>
#include <OtakuWheelers/Mediators/Cluster.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Application::Application;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Manager;
using TwistersFury::OtakuWheelers::DiscordBot::Logger::StandardOut;
using TwistersFury::OtakuWheelers::Mediators::Cluster;

namespace TwistersFury::OtakuWheelers::DiscordBot::Di {
    ManagerInterface & FactoryDefault::getCommandManager()
    {
        this->getLogger().debug("Get Command Manager");

        auto* manager = new Manager(this->config);

        manager->initialize();

        return *manager;
    }

    FactoryDefault::FactoryDefault(ConfigInterface &config): config{ config } {

    }

    ClusterInterface & FactoryDefault::getCluster()
    {
        if (!this->dppCluster) {
            this->getLogger().debug("Configuring Cluster", {{"token", this->config.getToken()}});

            this->dppCluster = new Cluster(this->config.getToken());
            this->dppCluster->on_log(dpp::utility::cout_logger());
        }

        this->getLogger().debug("Get Cluster");

        return *this->dppCluster;
    }

    ConfigInterface & FactoryDefault::getConfig() {
        this->getLogger().debug("Get Config");

        return this->config;
    }

    ApplicationInterface & FactoryDefault::getApplication() {
        if (this->application == nullptr) {
            this->getLogger().debug("Starting Application");

            this->application = new class Application(*this);
            this->application->initialize();
        }

        this->getLogger().debug("Get Application");

        return *this->application;
    }

    LoggerInterface & FactoryDefault::getLogger() {
        if (this->logger == nullptr) {
            this->logger = new StandardOut();

            this->logger->debug("Logging Started");
        }

        return *this->logger;
    }
}
