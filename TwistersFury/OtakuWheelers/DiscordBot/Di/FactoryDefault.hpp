//
// Created by Fenikkusu on 11/8/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_FACTORYDEFAULT_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_FACTORYDEFAULT_HPP

#include "Interface/DiInterface.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Di::Interface::DiInterface;

//TODO: Replace Factory With C++ Templates

namespace TwistersFury::OtakuWheelers::DiscordBot::Di {
    class FactoryDefault : public DiInterface
    {
        ConfigInterface& config;

        ClusterInterface* dppCluster = nullptr;
        ApplicationInterface* application = nullptr;
        LoggerInterface* logger = nullptr;

    public:
        explicit FactoryDefault(ConfigInterface& config);

        ApplicationInterface& getApplication() override;
        ManagerInterface& getCommandManager() override;
        ClusterInterface& getCluster() override;
        ConfigInterface& getConfig() override;
        LoggerInterface& getLogger() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_FACTORYDEFAULT_HPP
