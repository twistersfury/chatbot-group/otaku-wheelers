//
// Created by Fenikkusu on 11/8/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_INITIALIZATIONAWAREINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_INITIALIZATIONAWAREINTERFACE_HPP

namespace TwistersFury::OtakuWheelers::DiscordBot::Di::Interface {
    struct InitializationAwareInterface {
        virtual void initialize() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_INITIALIZATIONAWAREINTERFACE_HPP
