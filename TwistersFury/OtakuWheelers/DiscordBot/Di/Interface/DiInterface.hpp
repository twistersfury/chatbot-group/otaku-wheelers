//
// Created by Fenikkusu on 11/8/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_DIINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_DIINTERFACE_HPP

#include <OtakuWheelers/DiscordBot/Application/Interface/ApplicationInterface.hpp>
#include <OtakuWheelers/DiscordBot/Command/Interface/ManagerInterface.hpp>
#include <OtakuWheelers/DiscordBot/Logger/Interface/LoggerInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/ClusterInterface.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Application::Interface::ApplicationInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::ManagerInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Logger::Interface::LoggerInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Di::Interface {
    struct DiInterface {
        virtual ManagerInterface& getCommandManager() =0;
        virtual ConfigInterface& getConfig() =0;
        virtual ClusterInterface& getCluster() =0;
        virtual ApplicationInterface& getApplication() =0;
        virtual LoggerInterface& getLogger() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_DIINTERFACE_HPP
