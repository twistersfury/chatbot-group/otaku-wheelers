//
// Created by Fenikkusu on 11/5/23.
//

#include "Application.hpp"

#include <OtakuWheelers/DiscordBot/Dpp/CommandFactory.hpp>
#include <OtakuWheelers/DiscordBot/Command/Manager.hpp>
#include <OtakuWheelers/Mediators/Cluster.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Dpp::CommandFactory;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Manager;
using TwistersFury::OtakuWheelers::Mediators::Cluster;

namespace TwistersFury::OtakuWheelers::DiscordBot::Application {
    Application::Application(DiInterface &di) : di {di }{
        this->di.getLogger().info("Application Initialized");
    }

    void Application::run()
    {
        this->di.getLogger().info("Starting Bot");

        this->getCluster().start(dpp::st_wait);
    }

    void Application::initialize()
    {
        this->di.getLogger().info("Initializing Bot");

        this->getCluster().start_timer(
            [&](timer timerEvent) {
                this->di.getLogger().info("Application::initialize::startTimer::Triggered");
            },
            5
        );

        this->getCluster().on_slashcommand([&](const dpp::slashcommand_t& event) {
            this->di.getLogger().info("Slash Command Received");

            auto slashCommand = SlashCommand{event};
            this->onSlash(slashCommand);
        });

        this->getCluster().on_ready([&](const ready_t& event) {
            this->di.getLogger().info("Bot Ready");

            this->onReady();
        });
    }

    Application& Application::onReady()
    {
        // Test Valid Version of DPP run_once
        if (this->isReady) {
            return *this;
        }

        this->isReady = true;

        auto commandFactory = CommandFactory{};
        auto& manager = this->di.getCommandManager();

        for (auto const& [key, command] : manager.getCommands()) {
            this->getCluster().global_command_create(commandFactory.makeCommand(*command, this->getCluster()));
        }

        return *this;
    }

    Application& Application::onSlash(SlashCommandInterface& slashCommand) {
        auto& manager = this->di.getCommandManager();

        CommandInterface& command = manager.getCommand(slashCommand.getName());
        command.handleEvent(slashCommand);

        return *this;
    }

    /**
     * LazyLoad Method For Getting Cluster If Not Initialized On Construct
     *
     * @return ClusterInterface*
     */
    ClusterInterface& Application::getCluster()
    {
        return this->di.getCluster();
    }
}
