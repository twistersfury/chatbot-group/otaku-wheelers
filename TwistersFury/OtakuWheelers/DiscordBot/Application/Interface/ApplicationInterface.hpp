//
// Created by Fenikkusu on 11/9/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_APPLICATIONINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_APPLICATIONINTERFACE_HPP

#include <dpp/dpp.h>
#include <OtakuWheelers/DiscordBot/Config/Config.hpp>
#include <OtakuWheelers/DiscordBot/Di/Interface/InitializationAwareInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/ClusterInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/SlashCommandInterface.hpp>
#include <string>

using namespace dpp;

using TwistersFury::OtakuWheelers::DiscordBot::Config::Config;
using TwistersFury::OtakuWheelers::DiscordBot::Di::Interface::InitializationAwareInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::SlashCommandInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Application::Interface {
    struct ApplicationInterface: public InitializationAwareInterface
    {
        virtual void run() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_APPLICATIONINTERFACE_HPP
