//
// Created by Fenikkusu on 11/5/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_APPLICATION_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_APPLICATION_HPP

#include <dpp/dpp.h>
#include "Interface/ApplicationInterface.hpp"
#include <OtakuWheelers/DiscordBot/Config/Config.hpp>
#include <OtakuWheelers/DiscordBot/Di/Interface/DiInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/ClusterInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/SlashCommandInterface.hpp>
#include <string>

using namespace dpp;

using std::string;
using TwistersFury::OtakuWheelers::DiscordBot::Application::Interface::ApplicationInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Di::Interface::DiInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Config;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::SlashCommandInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Application {
    class Application: public ApplicationInterface {
        DiInterface& di;

        bool isReady = false;
    public:
        explicit Application(DiInterface& di);

        ClusterInterface& getCluster();
        Application& onReady();
        Application& onSlash(SlashCommandInterface& slashCommand);

        void run() override;
        void initialize() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_APPLICATION_HPP
