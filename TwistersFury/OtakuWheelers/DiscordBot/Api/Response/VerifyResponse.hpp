//
// Created by Fenikkusu on 11/2/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_VERIFY_RESPONSE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_VERIFY_RESPONSE_HPP

#include <nlohmann/json.hpp>
#include <OtakuWheelers/DiscordBot/Api/Interface/VerifyResponseInterface.hpp>

using nlohmann::json;
using namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Interface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Response {
    class VerifyResponse: public VerifyResponseInterface
    {
        bool verified = false;
        Errors errors = Errors::None;

    public:
        explicit VerifyResponse(json &jsonData);

        bool isVerified() override;
        Errors getError() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_VERIFY_RESPONSE_HPP
