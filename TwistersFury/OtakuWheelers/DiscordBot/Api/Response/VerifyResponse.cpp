//
// Created by Fenikkusu on 11/2/23.
//

#include "VerifyResponse.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Response {
    VerifyResponse::VerifyResponse(json &jsonData)
    {
        this->verified = jsonData["verified"].template get<bool>();
        this->errors = static_cast<Errors>(jsonData["error"].template get<int>());
    }

    bool VerifyResponse::isVerified()
    {
        return this->verified;
    }

    Errors VerifyResponse::getError()
    {
        return this->errors;
    }
}
