//
// Created by Fenikkusu on 11/2/23.
//

#include "Request.hpp"

#include <utility>
#include "Response/VerifyResponse.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Api::Response::VerifyResponse;
using TwistersFury::OtakuWheelers::Mediators::Curl;

namespace TwistersFury::OtakuWheelers::DiscordBot::Api {
    json Request::makeRequest(const string &url)
    {
        json payload = {
            {"_tf", "nope"}
        };

        return this->makeRequest(url, payload);
    }

    json Request::makeRequest(const string &url, json &payload)
    {
        return json::parse(
            this->curlInterface->postRequest(
                this->baseUrl + url,
                payload
            )
        );
    }

    VerifyResponseInterface* Request::verifyUser(const string &userName, const string &twoFactor)
    {
        json payload = {
            {"twoFactor", twoFactor}
        };

        json jsonData = this->makeRequest(
            "/discord/verify",
            payload
        );

        return new VerifyResponse{ jsonData };
    }

    Request::Request(string baseUrl, CurlInterface* curlInterface) : baseUrl{ std::move(baseUrl) }, curlInterface { curlInterface }
    {
        if (curlInterface == nullptr) {
            this->curlInterface = new Curl{};
        }
    }

    Request::Request(string baseUrl) : Request(std::move(baseUrl), nullptr) {

    }
}
