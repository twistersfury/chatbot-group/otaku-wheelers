//
// Created by Fenikkusu on 11/2/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_REQUEST_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_REQUEST_HPP

#include <nlohmann/json.hpp>
#include <string>
#include <OtakuWheelers/DiscordBot/Api/Interface/RequestInterface.hpp>
#include <OtakuWheelers/DiscordBot/Api/Interface/VerifyRequestInterface.hpp>
#include <OtakuWheelers/DiscordBot/Api/Response/VerifyResponse.hpp>
#include <OtakuWheelers/Mediators/Curl.hpp>

using nlohmann::json;
using std::string;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Interface::VerifyResponseInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Interface::VerifyRequestInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Interface::RequestInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::CurlInterface;
using TwistersFury::OtakuWheelers::Mediators::Curl;


namespace TwistersFury::OtakuWheelers::DiscordBot::Api {
    class Request: public VerifyRequestInterface, public RequestInterface {
        string baseUrl;
        CurlInterface* curlInterface;

    public:
        explicit Request(string baseUrl);
        Request(string baseUrl, CurlInterface* curlInterface);

        json makeRequest(const string& url) override;
        json makeRequest(const string& url, json &payload) override;
        VerifyResponseInterface* verifyUser(const string &userName, const string &twoFactor) override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_REQUEST_HPP
