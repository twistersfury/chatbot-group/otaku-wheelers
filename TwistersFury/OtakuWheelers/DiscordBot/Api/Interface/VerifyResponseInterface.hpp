//
// Created by Fenikkusu on 11/3/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_VERIFYRESPONSEINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_VERIFYRESPONSEINTERFACE_HPP

#include <nlohmann/json.hpp>
#include <string>

using nlohmann::json;
using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Interface {
    enum Errors {
        None = 0,
        Code = 1,
        NotFound = 2,
        Invalid = 4
    };

    struct VerifyResponseInterface {
        virtual bool isVerified() =0;
        virtual Errors getError() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_VERIFYRESPONSEINTERFACE_HPP
