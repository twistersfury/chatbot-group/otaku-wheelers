//
// Created by Fenikkusu on 11/3/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_VERIFYINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_VERIFYINTERFACE_HPP

#include <nlohmann/json.hpp>
#include <OtakuWheelers/DiscordBot/Api/Interface/VerifyResponseInterface.hpp>
#include <string>

using nlohmann::json;
using std::string;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Interface::VerifyResponseInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Interface {
    struct VerifyRequestInterface {
        virtual VerifyResponseInterface* verifyUser(const string &userName, const string &twoFactor) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_VERIFYINTERFACE_HPP
