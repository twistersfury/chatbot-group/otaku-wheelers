//
// Created by Fenikkusu on 11/3/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_REQUESTINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_REQUESTINTERFACE_HPP

#include <nlohmann/json.hpp>
#include <string>

using nlohmann::json;
using std::string;

namespace TwistersFury::OtakuWheelers::DiscordBot::Api::Interface {
    struct RequestInterface {
        virtual json makeRequest(const string& url) =0;
        virtual json makeRequest(const string& url, json &payload) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_REQUESTINTERFACE_HPP
