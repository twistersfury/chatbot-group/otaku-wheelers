//
// Created by Fenikkusu on 11/8/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_COMMANDFACTORY_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_COMMANDFACTORY_HPP

#include <dpp/dpp.h>
#include <OtakuWheelers/DiscordBot/Command/Interface/OptionInterface.hpp>
#include <OtakuWheelers/DiscordBot/Command/Interface/CommandInterface.hpp>
#include <OtakuWheelers/Mediators/Interface/ClusterInterface.hpp>

using dpp::slashcommand;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::CommandInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::OptionInterface;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Dpp {
    class CommandFactory {
    public:
        command_option makeOption(OptionInterface* option);
        slashcommand makeCommand(CommandInterface& command, ClusterInterface& cluster);
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_COMMANDFACTORY_HPP
