//
// Created by Fenikkusu on 11/8/23.
//

#include "CommandFactory.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Dpp {
    slashcommand CommandFactory::makeCommand(CommandInterface& command, ClusterInterface& dppCluster) {
        slashcommand slashCommand = dpp::slashcommand(
            command.getCommand(),
            command.getDescription(),
            dppCluster.getId()
        );

        for(auto& [optionKey, option] : command.getOptions()) {

            command_option commandOption = this->makeOption(option);

            slashCommand.add_option(commandOption);
        }

        return slashCommand;
    }

    command_option CommandFactory::makeOption(OptionInterface* option)
    {
        command_option commandOption = command_option(
            co_string,
            option->getName(),
            option->getDescription(),
            option->isRequired()
        );

        for (auto const& [optionValue, optionId] : option->getValues()) {
            commandOption.add_choice(
                command_option_choice(
                    optionValue,
                    optionId
                )
            );
        }

        return commandOption;
    }
}
