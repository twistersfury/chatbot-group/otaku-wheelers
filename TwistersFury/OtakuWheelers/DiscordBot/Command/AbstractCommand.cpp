//
// Created by Fenikkusu on 11/7/23.
//

#include "AbstractCommand.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    map<string, OptionInterface*> AbstractCommand::getOptions()
    {
        return this->options;
    }

    void AbstractCommand::addOption(OptionInterface *option)
    {
        this->options[option->getName()] = option;
    }
}
