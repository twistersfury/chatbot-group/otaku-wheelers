//
// Created by Fenikkusu on 11/8/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_MANAGERINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_MANAGERINTERFACE_HPP

#include <OtakuWheelers/DiscordBot/Command/Interface/CommandInterface.hpp>
#include <OtakuWheelers/DiscordBot/Config/Interface/ConfigInterface.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::CommandInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Interface::ConfigInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command::Interface {
    struct ManagerInterface {
        virtual CommandInterface & getCommand(const string& commandId) =0;
        virtual ManagerInterface & addCommand(CommandInterface* commandInterface) =0;
        virtual const std::map<string, CommandInterface*> & getCommands() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_MANAGERINTERFACE_HPP
