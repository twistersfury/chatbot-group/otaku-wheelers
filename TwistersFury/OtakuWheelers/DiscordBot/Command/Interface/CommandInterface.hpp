//
// Created by Fenikkusu on 10/31/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_COMMANDINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_COMMANDINTERFACE_HPP

#include <dpp/dpp.h>
#include <string>
#include <OtakuWheelers/DiscordBot/Command/Interface/OptionInterface.hpp>
#include <OtakuWheelers/Mediators/SlashCommand.hpp>
#include <map>

using std::map;
using std::string;
using dpp::slashcommand_t;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::OptionInterface;
using TwistersFury::OtakuWheelers::Mediators::SlashCommand;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command::Interface {
    struct CommandInterface
    {
        virtual string getCommand() =0;
        virtual string getDescription() =0;
        virtual void handleEvent(SlashCommandInterface& event) =0;
        virtual map<string, OptionInterface*> getOptions() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_COMMANDINTERFACE_HPP
