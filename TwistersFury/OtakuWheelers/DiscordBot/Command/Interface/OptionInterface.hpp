//
// Created by Fenikkusu on 10/31/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_OPTIONINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_OPTIONINTERFACE_HPP

#include <dpp/dpp.h>
#include <string>
#include <OtakuWheelers/Mediators/SlashCommand.hpp>
#include <map>

using std::map;
using std::string;
using dpp::slashcommand_t;
using TwistersFury::OtakuWheelers::Mediators::SlashCommand;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command::Interface {
    struct OptionInterface
    {
        virtual string getName() =0;
        virtual string getDescription() =0;
        virtual bool isRequired() =0;
        virtual map<string, string> getValues() =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_OPTIONINTERFACE_HPP
