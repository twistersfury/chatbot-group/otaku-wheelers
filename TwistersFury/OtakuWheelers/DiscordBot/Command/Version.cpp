//
// Created by Fenikkusu on 10/31/23.
//

#include "Version.hpp"

#include <OtakuWheelers/DiscordBot/Config/Version.hpp>

using ConfigVersion = TwistersFury::OtakuWheelers::DiscordBot::Config::Version;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    string Version::getCommand()
    {
        return "version";
    }

    string Version::getDescription()
    {
        return "Retrieve Current Bot Version";
    }

    void Version::handleEvent(SlashCommandInterface& event)
    {
        ConfigVersion configVersion{};

        event.reply(configVersion.getVersion());
    }
}
