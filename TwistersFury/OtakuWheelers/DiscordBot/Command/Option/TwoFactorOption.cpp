//
// Created by Fenikkusu on 11/7/23.
//

#include "TwoFactorOption.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Command::Option {
    string TwoFactorOption::getName()
    {
        return "code";
    }

    string TwoFactorOption::getDescription()
    {
        return "Verification Code From Website";
    }

    bool TwoFactorOption::isRequired()
    {
        return true;
    }

    map<string, string> TwoFactorOption::getValues()
    {
        return map<string, string>{};
    }
}
