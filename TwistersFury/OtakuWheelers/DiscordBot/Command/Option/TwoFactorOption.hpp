//
// Created by Fenikkusu on 11/7/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_TWOFACTOROPTION_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_TWOFACTOROPTION_HPP

#include "../Interface/OptionInterface.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::OptionInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command::Option {
    class TwoFactorOption: public OptionInterface
    {
    public:
        string getName() override;
        string getDescription() override;
        bool isRequired() override;
        map<string, string> getValues() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_TWOFACTOROPTION_HPP
