//
// Created by Fenikkusu on 10/31/23.
//

#include "PingPong.hpp"

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    string PingPong::getCommand()
    {
        return "ping";
    }

    string PingPong::getDescription()
    {
        return "Ping pong!";
    }

     void PingPong::handleEvent(SlashCommandInterface& event)
    {
        event.reply("Pong!");
    }
}