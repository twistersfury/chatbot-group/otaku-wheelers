//
// Created by Fenikkusu on 11/1/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_VERIFYCOMMAND_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_VERIFYCOMMAND_HPP

#include <OtakuWheelers/DiscordBot/Api/Request.hpp>
#include <OtakuWheelers/DiscordBot/Command/AbstractCommand.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Command::AbstractCommand;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Interface::VerifyRequestInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    class VerifyCommand: public AbstractCommand
    {
        VerifyRequestInterface* apiRequest;

    public:
        explicit VerifyCommand(VerifyRequestInterface &request);

        string getCommand() override;
        string getDescription() override;
        void handleEvent(SlashCommandInterface& event) override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_VERIFYCOMMAND_HPP
