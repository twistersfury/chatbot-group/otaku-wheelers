//
// Created by Fenikkusu on 10/31/23.
//

#include "Manager.hpp"

#include <OtakuWheelers/DiscordBot/Api/Request.hpp>
#include <OtakuWheelers/DiscordBot/Command/PingPong.hpp>
#include <OtakuWheelers/DiscordBot/Command/VerifyCommand.hpp>
#include <OtakuWheelers/DiscordBot/Command/Version.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Api::Request;
using TwistersFury::OtakuWheelers::DiscordBot::Command::PingPong;
using TwistersFury::OtakuWheelers::DiscordBot::Command::VerifyCommand;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Version;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    Manager::Manager(ConfigInterface& config): config { config }
    {

    }

    CommandInterface & Manager::getCommand(const string& commandId)
    {
        return *this->commands[commandId];
    }

    ManagerInterface & Manager::addCommand(CommandInterface* commandInterface) {
        this->commands[commandInterface->getCommand()] = commandInterface;

        return *this;
    }

    const std::map<string, CommandInterface*> & Manager::getCommands()
    {
        return this->commands;
    }

    void Manager::initialize() {
        auto *apiRequest = new Request(config.getApiBaseUrl());
        auto *pingPong = new PingPong();
        auto *version = new Version();
        auto *verify = new VerifyCommand{*apiRequest};

        this->addCommand(pingPong);
        this->addCommand(version);
        this->addCommand(verify);
    }
}
