//
// Created by Fenikkusu on 10/31/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_PINGPONG_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_PINGPONG_HPP

#include <OtakuWheelers/DiscordBot/Command/AbstractCommand.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Command::AbstractCommand;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    class PingPong: public AbstractCommand {
    public:
        string getCommand() override;
        string getDescription() override;

        void handleEvent(SlashCommandInterface& event) override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_PINGPONG_HPP
