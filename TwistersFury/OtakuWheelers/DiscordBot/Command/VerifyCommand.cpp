//
// Created by Fenikkusu on 11/1/23.
//

#include "VerifyCommand.hpp"
#include <OtakuWheelers/DiscordBot/Api/Response/VerifyResponse.hpp>
#include "Option/TwoFactorOption.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Command::Option::TwoFactorOption;
using TwistersFury::OtakuWheelers::DiscordBot::Api::Response::VerifyResponse;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    string VerifyCommand::getCommand()
    {
        return "verify";
    }

    string VerifyCommand::getDescription()
    {
        return "Website Verification";
    }

    void VerifyCommand::handleEvent(SlashCommandInterface& event)
    {
        VerifyResponseInterface* response = this->apiRequest->verifyUser("userName", event.getParameter("code"));

        if (response->isVerified()) {
            event.reply("Thank You! Your verification is complete!");

            return;
        }

        switch(response->getError()) {
            case Errors::Code:
                event.reply("Invalid Verification Code");
                break;
            case Errors::NotFound:
                event.reply("You do not appear to have any pending verifications.");
                break;
            case Errors::Invalid:
                event.reply("You've submitted an invalid verification code.");
                break;
            default:
                event.reply("An unrecognized error has occurred.");
        }
    }

    VerifyCommand::VerifyCommand(VerifyRequestInterface &apiRequest) : apiRequest {&apiRequest } {
        auto* twoFactor = new TwoFactorOption();

        this->addOption(twoFactor);
    }
}
