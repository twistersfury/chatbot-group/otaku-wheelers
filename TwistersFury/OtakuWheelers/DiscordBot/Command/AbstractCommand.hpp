//
// Created by Fenikkusu on 11/7/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_ABSTRACTCOMMAND_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_ABSTRACTCOMMAND_HPP

#include "Interface/CommandInterface.hpp"

using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::CommandInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    class AbstractCommand: public CommandInterface {
        map<string, OptionInterface*> options = {};

    public:
        map<string, OptionInterface*> getOptions() override;
        void addOption(OptionInterface* option);
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_ABSTRACTCOMMAND_HPP
