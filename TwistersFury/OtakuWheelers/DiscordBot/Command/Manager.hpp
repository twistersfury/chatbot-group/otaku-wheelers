//
// Created by Fenikkusu on 10/31/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_MANAGER_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_MANAGER_HPP

#include "Interface/ManagerInterface.hpp"
#include <OtakuWheelers/DiscordBot/Di/Interface/InitializationAwareInterface.hpp>

using TwistersFury::OtakuWheelers::DiscordBot::Di::Interface::InitializationAwareInterface;
using TwistersFury::OtakuWheelers::DiscordBot::Command::Interface::ManagerInterface;

namespace TwistersFury::OtakuWheelers::DiscordBot::Command {
    class Manager : public ManagerInterface, InitializationAwareInterface
    {
        ConfigInterface& config;
        std::map<string, CommandInterface*> commands;

    public:
        explicit Manager(ConfigInterface &config);

        CommandInterface & getCommand(const string& commandId) override;
        ManagerInterface & addCommand(CommandInterface* commandInterface) override;
        const std::map<string, CommandInterface*> & getCommands() override;

        void initialize() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_MANAGER_HPP
