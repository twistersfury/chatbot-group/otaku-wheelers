//
// Created by Fenikkusu on 11/6/23.
//

#include "Cluster.hpp"

namespace TwistersFury::OtakuWheelers::Mediators {
    Cluster::Cluster(const string& token)
    {
        this->dppBot = new cluster(token);
    }

    snowflake Cluster::getId()
    {
        return this->dppBot->me.id;
    }

    void Cluster::global_command_create(const slashcommand &slashCommand)
    {
        this->dppBot->global_command_create(slashCommand);
    }


    void Cluster::on_log(std::function<void(const dpp::log_t&)> log)
    {
        this->dppBot->on_log(log);
    }

    void Cluster::on_ready(std::function<void(const dpp::ready_t&)> onReady)
    {
        this->dppBot->on_ready(onReady);
    }

    void Cluster::on_slashcommand(std::function<void(const dpp::slashcommand_t&)> slashCommand)
    {
        this->dppBot->on_slashcommand(slashCommand);
    }

    void Cluster::start(bool returnAfter)
    {
        this->dppBot->start(returnAfter);
    }

    timer Cluster::start_timer(timer_callback_t on_tick, uint64_t frequency, timer_callback_t on_stop)
    {
        return this->dppBot->start_timer(on_tick, frequency, on_stop);
    }
}
