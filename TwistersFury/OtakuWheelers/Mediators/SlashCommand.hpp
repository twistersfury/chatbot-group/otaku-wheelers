//
// Created by Fenikkusu on 11/2/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMAND_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMAND_HPP

#include <dpp/dpp.h>
#include <string>
#include <OtakuWheelers/Mediators/Interface/SlashCommandInterface.hpp>

using dpp::slashcommand_t;
using std::string;
using TwistersFury::OtakuWheelers::Mediators::Interface::SlashCommandInterface;

namespace TwistersFury::OtakuWheelers::Mediators {
    class SlashCommand : public SlashCommandInterface {
        slashcommand_t slashCommand;
    public:
        explicit SlashCommand(slashcommand_t slashCommand);

        void reply(string message) override;
        string getName() override;
        string getParameter(const string paramName) override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMAND_HPP
