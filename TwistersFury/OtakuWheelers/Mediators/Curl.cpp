//
// Created by Fenikkusu on 11/4/23.
//

#include "Curl.hpp"
#include <cpr/cpr.h>

using namespace cpr;

namespace TwistersFury::OtakuWheelers::Mediators {
    string Curl::postRequest(string requestUrl, json postData)
    {
        Response response = Post(
            Url{requestUrl},
            Body{ postData.dump() },
            Header {
                {"Content-Type", "application/json"}
            }
        );

        return response.text;
    }
}
