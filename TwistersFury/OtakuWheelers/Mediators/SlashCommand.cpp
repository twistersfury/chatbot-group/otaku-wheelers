//
// Created by Fenikkusu on 11/2/23.
//

#include "SlashCommand.hpp"

#include <utility>

namespace TwistersFury::OtakuWheelers::Mediators {
    SlashCommand::SlashCommand(dpp::slashcommand_t slashCommand) : slashCommand {std::move( slashCommand )} {

    }

    void SlashCommand::reply(const string message) {
        this->slashCommand.reply(message);
    }

    string SlashCommand::getName()
    {
        return this->slashCommand.command.get_command_name();
    }

    string SlashCommand::getParameter(const string paramName)
    {
        return std::get<std::string>(this->slashCommand.get_parameter(paramName));
    }
}
