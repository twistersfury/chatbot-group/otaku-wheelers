//
// Created by Fenikkusu on 11/6/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CLUSTER_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CLUSTER_HPP

#include <dpp/dpp.h>
#include <string>
#include <OtakuWheelers/Mediators/Interface/ClusterInterface.hpp>

using namespace dpp;
using std::string;
using TwistersFury::OtakuWheelers::Mediators::Interface::ClusterInterface;


namespace TwistersFury::OtakuWheelers::Mediators {
    class Cluster: public ClusterInterface
    {
        cluster* dppBot;
    public:
        explicit Cluster(const string& token);

        void on_log(std::function<void(const dpp::log_t&)> log) override;
        void start(bool returnAfter) override;
        void on_slashcommand(std::function<void(const dpp::slashcommand_t&)> slashCommand) override;
        void on_ready(std::function<void(const dpp::ready_t&)> onReady) override;
        void global_command_create(const slashcommand &slashCommand) override;
        timer start_timer(timer_callback_t on_tick, uint64_t frequency, timer_callback_t on_stop = {}) override;
        snowflake getId() override;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CLUSTER_HPP
