//
// Created by Fenikkusu on 11/4/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_MEDIATOR_CURL_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_MEDIATOR_CURL_HPP

#include <OtakuWheelers/Mediators/Interface/CurlInterface.hpp>

using TwistersFury::OtakuWheelers::Mediators::Interface::CurlInterface;

namespace TwistersFury::OtakuWheelers::Mediators {
    class Curl: public CurlInterface {
    public:
        string postRequest(string requestUrl, json postData);
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_MEDIATOR_CURL_HPP
