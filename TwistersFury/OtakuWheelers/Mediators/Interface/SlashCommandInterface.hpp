//
// Created by Fenikkusu on 11/2/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMANDINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMANDINTERFACE_HPP

#include <string>

using std::string;

namespace TwistersFury::OtakuWheelers::Mediators::Interface {
    struct SlashCommandInterface {
        virtual void reply(const string message) =0;
        virtual string getName() =0;
        virtual string getParameter(const string paramName) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_SLASHCOMMANDINTERFACE_HPP
