//
// Created by Fenikkusu on 11/4/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CURLINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CURLINTERFACE_HPP

#include <string>
#include <nlohmann/json.hpp>

using std::string;
using nlohmann::json;

namespace TwistersFury::OtakuWheelers::Mediators::Interface {
    struct CurlInterface {
        virtual string postRequest(string requestUrl, json postData) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CURLINTERFACE_HPP
