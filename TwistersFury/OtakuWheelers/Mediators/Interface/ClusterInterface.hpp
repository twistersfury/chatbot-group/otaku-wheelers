//
// Created by Fenikkusu on 11/4/23.
//

#ifndef OTAKUWHEELERS_CLUB_DISCORD_CLUSTERINTERFACE_HPP
#define OTAKUWHEELERS_CLUB_DISCORD_CLUSTERINTERFACE_HPP

#include <dpp/dpp.h>

using namespace dpp;

namespace TwistersFury::OtakuWheelers::Mediators::Interface {
    struct ClusterInterface {
        virtual snowflake getId() =0;
        virtual void global_command_create(const slashcommand &slashCommand) =0;
        virtual void on_log(std::function<void(const dpp::log_t&)> log) =0;
        virtual void on_ready(std::function<void(const dpp::ready_t&)> onReady) =0;
        virtual void on_slashcommand(std::function<void(const dpp::slashcommand_t&)> slashCommand) =0;
        virtual void start(bool returnAfter) =0;
        virtual timer start_timer(timer_callback_t on_tick, uint64_t frequency, timer_callback_t on_stop = {}) =0;
    };
}

#endif //OTAKUWHEELERS_CLUB_DISCORD_CLUSTERINTERFACE_HPP
