//
// Created by Fenikkusu on 11/15/23.
//

#ifndef TF_VERSION
#define TF_VERSION

#include <string>

using std::string;

/**
 * This only exists as for CI/CD purposes. The intent is for the CI/CD to write this to a file and then compile the project.
 */
struct RawVersion {
    const string major = "0";
    const string minor = "6";
    const string patch = "0";
};

#endif
