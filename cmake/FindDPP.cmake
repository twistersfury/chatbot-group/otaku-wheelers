find_path(DPP_INCLUDE_DIR NAMES dpp/dpp.h HINTS /usr/local/include)

find_library(DPP_LIBRARIES NAMES dpp "libdpp.a" HINTS /usr/local/include)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(DPP DEFAULT_MSG DPP_LIBRARIES DPP_INCLUDE_DIR)