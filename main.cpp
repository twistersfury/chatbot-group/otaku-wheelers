#include <iostream>
#include <OtakuWheelers/DiscordBot/Config/Loader.hpp>
#include <OtakuWheelers/DiscordBot/Config/Version.hpp>
#include <OtakuWheelers/DiscordBot/Di/FactoryDefault.hpp>
#include <stdexcept>

using std::cout;
using std::runtime_error;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Loader;
using TwistersFury::OtakuWheelers::DiscordBot::Config::Version;
using TwistersFury::OtakuWheelers::DiscordBot::Di::FactoryDefault;

int main(int argc, char *argv[]) {
    if (argc <= 1) {
        throw runtime_error("Invalid Arguments");
    }

    auto version = Version{};
    cout << "Otaku Wheelers Discord Bot: " << version.getVersion() << std::endl;

    string command = argv[1];
    string configPath = "";
    if ("version" == command) {
        exit(0);
    } else if (argc == 2) {
        configPath = argv[1];
    } else {
        configPath = argv[2];
    }
    try {
        cout << "Starting Bot" << std::endl;

        auto configLoader = Loader{configPath};
        auto appConfig = Config{configLoader.loadConfig()};
        auto di = FactoryDefault{appConfig};

        di.getApplication().run();
    } catch (dpp::connection_exception& exception) {
        std::cerr << exception.what() << std::endl;
    } catch (...) {
        std::cerr << "Caught unknown exception." << std::endl;
    }
}
