#!/usr/bin/env bash

set -e; shopt -s nullglob

if [[ -z ${CC+x} ]]
then
  (>&1 echo "The CC variable is unset or set to the empty string.")
  (>&1 echo "Skip precompiling headers.")
else
  (>&1 echo "Creating precompiled headers...")
  if [[ "${CC:0:5}" = "clang" ]]
  then
    _ext="pch"
    _option="-emit-pch"
    _arg="-cc1"
  else
    _ext="ghc"
    _option=
    _arg=
  fi

	# If a `*.gch' (or a `*.pch') file is not found then the normal header files
	# will be used. For more see: http://en.wikipedia.org/wiki/Precompiled_header
  while IFS= read -r -d '' file
  do
    # shellcheck disable=SC2046
    ${CC} ${_arg} "${file}" -I. -I./ext ${_option} -o "${file}.${_ext}"
  done <   <(find ./ext/kernel -name '*.h' -print0)
fi

if [[ -n ${ENV_REPORT_COVERAGE+x} ]] && [[ "$ENV_REPORT_COVERAGE" = "1" ]]; then
  CFLAGS=${CFLAGS//-O[0-9s]/}
  CXXFLAGS=${CXXFLAGS//-O[0-9s]/}
  LDFLAGS=${LDFLAGS//--coverage/}

  LDFLAGS="${LDFLAGS} --coverage"
  CFLAGS="${CFLAGS} -O0 -ggdb -fprofile-arcs -ftest-coverage"
  CXXFLAGS="${CXXFLAGS} -O0 -ggdb -fprofile-arcs -ftest-coverage"

  export CPPFLAGS="-DCOVERAGE"
  export EXTRA_LDFLAGS='-precious-files-regex \.gcno\$$'
fi

export LD_LIBRARY_PATH=/usr/local/lib

cmake -S ${ENV_WORKDIR} -B ${ENV_WORKDIR}/build \
      -DDPP_BUILD_TEST=OFF \
      -DCMAKE_BUILD_TYPE=${ENV_BUILD_TYPE} ${ENV_BUILD_OPTIONS}

cmake --build ${ENV_WORKDIR}/build --parallel ${ENV_PARALLEL_JOBS}

#if [[ -n ${ENV_REPORT_COVERAGE+x} ]] && [[ "$ENV_REPORT_COVERAGE" = "1" ]]; then
#    cmake --build ${ENV_WORKDIR}/build --parallel 1 --target ${ENV_APP_NAME}-tests
#fi

cmake --install ${ENV_WORKDIR}/build

pushd ${ENV_WORKDIR}/build

    make install

popd
